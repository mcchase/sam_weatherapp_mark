import json
from botocore.vendored import requests

def lambda_handler(event, context):
    DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"
    queryStringParameters = event['queryStringParameters']

    latitude = queryStringParameters['lat']
    longitude = queryStringParameters['lon']
    city = queryStringParameters['city']
    time = queryStringParameters.get('time')
    print(time)
    if time == None:
        r = requests.get(f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude}')
        if r.status_code != 200:
            raise Exception('Invalid location')
        currently = r.json()['currently']
        daily = r.json()['daily']['data'][0]
    else:
        r = requests.get(f'https://api.darksky.net/forecast/{DARK_SKY_SECRET_KEY}/{latitude},{longitude},{time}')
        if r.status_code != 200:
            raise Exception('Invalid time format')
        currently = r.json()['currently']
        daily = r.json()['daily']['data'][0]

    res={
        'city': city,
        'summary': currently['summary'],
        'temperature': currently['temperature'],
        'high': daily['temperatureHigh'],
        'low': daily['temperatureLow'],
        'humidity': daily['humidity'],
        'real_feel': currently['apparentTemperature']
    }
    return {
        'statusCode': 200,
        'body': json.dumps(res)
    }