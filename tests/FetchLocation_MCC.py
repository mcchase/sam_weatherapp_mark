import json
# from botocore.vendored import requests
import requests

def lambda_handler(event, context):
    ip = event
    # ip = event['queryStringParameters']['ip']
    r = requests.get(f'https://ipvigilante.com/{ip}', verify=False).json()
    lat = r.get('data').get('latitude')
    lon = r['data']['longitude']
    city = r['data']['city_name']


    res = { "lat": lat, "lon": lon, "city": city }
    return {
        'statusCode': 200,
        'body': json.dumps(res)
    }
