import unittest
from FetchLocation_MCC import lambda_handler


class TestGetLocation(unittest.TestCase):
    def test_get_cords(self):
        event = {
            "queryStringParameters":
                {
                    "ip": '8.8.8.8'
                }
        }
        res = lambda_handler(event['queryStringParameters']['ip'], {})

        self.assertEqual(res['statusCode'], 200)


if __name__ == '__main__':
    unittest.main()