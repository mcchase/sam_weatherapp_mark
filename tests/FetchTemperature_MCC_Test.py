import unittest
from FetchTemperature_MCC import lambda_handler

class TestGetWeather(unittest.TestCase):
    def test_get_weather(self):
        mock_api_call = {
            'queryStringParameters': {
                'lat': '42.3601',
                'lon': '71.0589',
                'city': 'Boston, MA',
                'time': None
            }
        }

        res = lambda_handler(mock_api_call)
        self.assertEqual(res['statusCode'], 200)


if __name__ == '__main__':
    unittest.main()