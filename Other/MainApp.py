import requests


def get_location(ip):
    res = requests.get(f"https://he0dbys7pa.execute-api.us-east-1.amazonaws.com/Prod/location?ip={ip}", verify=False).json()
    return res


def get_weather(lat, lon, city):
    res = requests.get(f"https://he0dbys7pa.execute-api.us-east-1.amazonaws.com/Prod/weather?city={city}&lat={lat}&lon={lon}", verify=False).json()
    return res


def print_forecast(weather_object):
    location = weather_object['city']
    summary = weather_object['summary']
    temp = weather_object['temperature']
    high = weather_object['high']
    low = weather_object['low']
    humidity = weather_object['humidity']
    real_feel = weather_object['real_feel']
    print(f'The weather forecast in {location} is {summary}\n')
    print(f'{temp}\n')
    print(f'Temperature: {temp}')
    print(f'High: {high}')
    print(f'Low: {low}')
    print(f"Humidity: {humidity}")
    print(f"Feels like: {real_feel}")


if __name__ == "__main__":
    #longitude, latitude, city = get_location()
    #weather_object = get_temperature(longitude, latitude, city, user_defined_time)
    #print_forecast(weather_object)

    cords = get_location("8.8.8.8")
    lat = cords['lat']
    lon = cords['lon']
    city = cords['city']
    weather = get_weather(lat, lon, city)
    print_forecast(weather)