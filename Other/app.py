#! /usr/bin/env python3
import requests
import argparse
import os

parser = argparse.ArgumentParser(description="This program takes a time and returns the weather for that time or the current weather by default")
parser.add_argument("--time")
args = parser.parse_args()
user_defined_time = args.time

kub_time = os.getenv('TIME', "default")

DARK_SKY_SECRET_KEY="fbcf15d3d872f2f66720690d60e26343"


def get_location():
    """ Returns the longitude and latitude for the location of this machine."""
    req = requests.request("GET", 'https://ipvigilante.com')

    location = req.json() # deserialize/parse json response to dictionary

    longitude = location["data"]['longitude']
    latitude = location["data"]['latitude']
    city = location["data"]['city_name']

    return latitude, longitude, city


def get_temperature(latitude, longitude, city, time):
    """ Returns the current temperature at the specified location """
    if time == None:
        req2 = requests.request('GET', f'https://api.darksky.net/forecast/fbcf15d3d872f2f66720690d60e26343/{latitude},{longitude}')
        if req2.status_code != 200:
            raise Exception("Location not found")
        forecast = req2.json()
    else:
        req2 = requests.request('GET', f'https://api.darksky.net/forecast/fbcf15d3d872f2f66720690d60e26343/{latitude},{longitude},{time}')
        if req2.status_code != 200:
            raise Exception("Invalid time")
        forecast = req2.json()

    summary = forecast['currently']['summary']
    temp = forecast['currently']['temperature']
    high = forecast['daily']['data'][0]['temperatureHigh']
    low = forecast['daily']['data'][0]['temperatureLow']
    humidity = forecast['currently']['humidity']
    feelslike = forecast['currently']['apparentTemperature']

    return city, summary, temp, high, low, humidity, feelslike

def print_forecast(city, summary, temp, high, low, humidity, feelslike):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("City:", city)
    print("Forecast:", summary)
    print("Temperature:", temp)
    print("High:", high)
    print("Low:", low)
    print("Humidity:", humidity)
    print("Feels Like:", feelslike)


if __name__ == "__main__":
    longitude, latitude, city = get_location()
    city, summary, temp, high, low, humidity, feelslike = get_temperature(longitude, latitude, city, user_defined_time)
    print_forecast(city, summary, temp, high, low, humidity, feelslike)




